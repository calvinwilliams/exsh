FILE=$1

cat ${FILE} | \
	sed 's/^\(.*\)FATALLOG( \(.*\))$/\1FatalLog( __FILE__ , __LINE__ , \2);/g' | \
	sed 's/^\(.*\)ERRORLOG( \(.*\))$/\1ErrorLog( __FILE__ , __LINE__ , \2);/g' | \
	sed 's/^\(.*\)WARNLOG( \(.*\))$/\1WarnLog( __FILE__ , __LINE__ , \2);/g' | \
	sed 's/^\(.*\)INFOLOG( \(.*\))$/\1InfoLog( __FILE__ , __LINE__ , \2);/g' | \
	sed 's/^\(.*\)DebugLOG( \(.*\))$/\1DebugLog( __FILE__ , __LINE__ , \2);/g' >${FILE}.tmp
mv ${FILE}.tmp ${FILE}

