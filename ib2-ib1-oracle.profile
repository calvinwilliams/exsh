# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PROJECT_DIR=$HOME;export PROJECT_DIR
PATH=.:$PATH:$HOME/.local/bin:$HOME/bin; export PATH









#################################################
# for basic

OSNAME=`uname -a|awk '{print $1}'`
HOSTNAME=`hostname`
USERNAME=$LOGNAME
export PS1='[$USERNAME@$HOSTNAME $PWD] '

set -o vi
ulimit -c unlimited

export PATH=.:$HOME/exbin:$HOME/exsh:$HOME/bin:$HOME/bin.tools:$HOME/bin.cli:$HOME/bin.server:$HOME/shbin:$HOME/etc:$PATH
export PATH=$PATH:$HOME/mktpl

if [ x"$OSNAME" = x"Linux" ] ; then
        export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/exlib:$HOME/lib:$HOME/so
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/mqm/lib64:/opt/mqm/lib64
elif [ x"$OSNAME" = x"AIX" ] ; then
        export LIBPATH=.:$LIBPATH
        export LIBPATH=$LIBPATH:$HOME/exlib:$HOME/lib:$HOME/so
        export LIBPATH=$LIBPATH:/usr/mqm/lib64
        export OBJECT_MODE=64
fi

export LC_ALL=zh_CN.gb18030

alias l='ls -l'
alias ll='ls -lF'
alias lf='ls -F'
alias lrt='ls -lrt'

alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'

alias view='vi -R'

unset SSH_ASKPASS

#################################################
# for mktpl

export MKTPLDIR=$HOME/mktpl
export MKTPLOS=`uname -a | awk '{print $1}'`

#################################################
# for mktpl2
export MKTPL2_HOME=$HOME/mktpl2
export MKTPL2_OS=`uname -a | awk '{print $1}'`
export PATH=${MKTPL2_HOME}:$PATH

################################################
# for database

. $HOME/etc/SetOracleEnv
. $HOME/etc/DBSetEnv
export PROJECT_DIR=$HOME

################################################
# for IB2

export IB2_SODIR=$HOME/modules
export IB2_FILEDIR=$HOME/print

################################################
# for dc4c,hzbat

export DC4C_ACT=act
export DC4C_OWSERVER=dc4c_owserver

################################################
# for SQLST

# export SQLST_HOSTNAME=
export SQLST_USERNAME=heronsdb
export SQLST_PASSWORD=heronsdb

################################################
# for svn

export SVN_EDITOR=vi

################################################
# for IBP

export IBP_BINDIR=$HOME/bin
export IBP_SODIR=$HOME/modules
export IBP_FILEDIR=$HOME/print

export SSH_CLUSTER_PARALLEL=1

