function make_make()
{
	CPUS=`cat /proc/cpuinfo | grep -w processor | wc -l`
	make -j $CPUS $*
	NRET=$?
	if [ $NRET -ne 0 ] ; then
		exit $NRET
	fi
}

if [ -f makes ] ; then
	for DIR in `cat makes` ; do
		cd $DIR
		make_make
		cd ..
	done
else
	make_make
fi

