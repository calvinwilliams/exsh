usage()
{
	echo "USAGE : acore_authserver.do [ start | stop | restart | update ]"
}

run()
{
	echo "$*"
	$*
}

if [ $# -eq 0 ] ; then
	usage
	exit 7 ;
fi

case $1 in
	start)
		echo "--------- ����authserver ---------"
		tmux new -s authserver -d
		tmux send -t authserver "cd ~/azeroth-server/bin && ./authserver" ENTER
		;;
	stop)
		echo "--------- ֹͣauthserver ---------"
		tmux kill-session -t authserver
		;;
	restart)
		acore_authserver.do stop
		sleep 1
		acore_authserver.do start
		;;
	update)
		echo "--------- ����authserver ---------"
		if [ -f $HOME/azeroth-server/bin.update/authserver ] ; then
			run rm -f $HOME/azeroth-server/bin/authserver
			run mv -f $HOME/azeroth-server/bin.update/authserver $HOME/azeroth-server/bin/authserver
		fi
esac

