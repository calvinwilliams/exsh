usage()
{
	echo "USAGE : redis.do [ start | stop | kill | restart | status | cmd ]"
}

run()
{
	echo "$*"
	$*
}

if [ $# -eq 0 ] ; then
	usage
	exit 7 ;
fi

case $1 in
	start)
		echo "--------- ����redis ---------"
		run "redis-server $HOME/etc/redis.conf"
		;;
	stop)
		echo "--------- ֹͣredis ---------"
		PORT=`grep -E "^port" $HOME/etc/redis.conf | grep -v grep | awk '{print $2}'`
		if [ x"$PORT" != x"" ] ; then
			run redis-cli -p $PORT shutdown
		fi
		;;
	kill)
		echo "--------- ɱ��redis ---------"
		run "kill -9 `cat $HOME/redisdata/redis.pid`"
		;;
	restart)
		redis.do stop
		sleep 1
		redis.do start
		;;
	status)
		echo "--------- redis���� ---------"
		ps -ef | grep "redis-server" | grep -v grep
		echo "--------- redis�˿� �---------"
		PORT=`grep -E "^port" $HOME/etc/redis.conf | grep -v grep | awk '{print $2}'`
		if [ x"$PORT" != x"" ] ; then
			netstat -an | grep -w $PORT | grep -v grep
		fi
		;;
	cmd)
		PORT=`grep -E "^port" $HOME/etc/redis.conf | grep -v grep | awk '{print $2}'`
		if [ x"$PORT" != x"" ] ; then
			redis-cli -p $PORT
		else
			redis-cli
		fi
		;;
	*)
		usage
		;;
esac

