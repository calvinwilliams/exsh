#!/bin/bash

if [ -d $HOME/exsh ] ; then
	echo "============================================================ git branch exsh ..."
	cd $HOME/exsh
	git branch
	if [ $? -ne 0 ] ; then
		echo "============================================================ git branch exsh failed"
		exit 1
	else
		echo "============================================================ git branch exsh ok"
	fi
fi

if [ -d $HOME/mktpl2 ] ; then
	echo "============================================================ git branch mktpl2 ..."
	cd $HOME/mktpl2
	git branch
	if [ $? -ne 0 ] ; then
		echo "============================================================ git branch mktpl2 failed"
		exit 1
	else
		echo "============================================================ git branch mktpl2 ok"
	fi
fi

grep -v -E "^#" $HOME/etc/mm_src_all.conf | while read GITREPO ; do
	if [ -d $HOME/src/$GITREPO ] ; then
		echo "============================================================ git branch $GITREPO ..."
		cd $HOME/src/$GITREPO
		git branch
		if [ $? -ne 0 ] ; then
			echo "============================================================ git branch $GITREPO failed"
			exit 1
		else
			echo "============================================================ git branch $GITREPO ok"
		fi
	fi
done

