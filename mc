function make_clean()
{
	make clean $*
	NRET=$?
	if [ $NRET -ne 0 ] ; then
		exit $NRET
	fi
}

if [ -f makes ] ; then
	for DIR in `cat makes` ; do
		cd $DIR
		make_clean
		cd ..
	done
else
	make_clean
fi

