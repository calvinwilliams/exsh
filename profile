#################################################
# for basic
export OSNAME=`uname -a|awk '{print $1}'`
export HOSTNAME=`hostname`
export USERNAME=$LOGNAME
export PS1='\033[1;36m[$USERNAME@$HOSTNAME $PWD]\033[0m '
export CPUS=`cat /proc/cpuinfo | grep processor | wc -l`

set -o vi

ulimit -c unlimited

export MAKEFLAGS="-j $CPUS"

# export LC_ALL=en_US
# export LANG=en_US
# export LC_ALL=zh_CN.gb18030
# export LANG=zh_CN.gb18030
export LC_ALL=C
export LANG=C

export PATH=.:$PATH:/usr/local/bin
export PATH=$PATH:$HOME/bin:$HOME/shbin:$HOME/exbin:$HOME/exsh
export PATH=$PATH:$HOME/bin.tools:$HOME/bin.cli:$HOME/bin.server

if [ x"$OSNAME" = x"Linux" ] ; then
	export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/exlib:$HOME/lib
elif [ x"$OSNAME" = x"AIX" ] ; then
	export LIBPATH=.:$LIBPATH
	export LIBPATH=$LIBPATH:$HOME/exlib:$HOME/lib
	export OBJECT_MODE=64
fi

alias l='ls -l'
alias ll='ls -lF'
alias lf='ls -F'
alias lrt='ls -lrt'

alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'

alias vi='vim'
alias view='vi -R'

unset SSH_ASKPASS

which lsb_release >/dev/null 2>&1
if [ $? -eq 0 ] ; then
	DISTRIBUTOR_ID=`lsb_release -a | grep "Distributor ID:" | awk '{print $3}'`
	RELEASE_VERSION=`lsb_release -a | grep "Release:" | awk '{print $2}' | awk -F. '{printf "%d.%d",$1,$2}'`
fi

if [ -f /etc/centos-release ] ; then
	DISTRIBUTOR_ID=`cat /etc/centos-release | awk '{print $1}'`
	RELEASE_VERSION=`cat /etc/centos-release | awk '{print $4}' | awk -F. '{printf "%d.%d",$1,$2}'`
fi

if [ x"${DISTRIBUTOR_ID}" != x"" ] ; then
	export PATH=$PATH:$HOME/exsh/${DISTRIBUTOR_ID}-${RELEASE_VERSION}-install
fi

#################################################
# for IBMMQ
if [ x"$OSNAME" = x"Linux" ] ; then
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/mqm/lib64:/opt/mqm/lib64
elif [ x"$OSNAME" = x"AIX" ] ; then
	export LIBPATH=$LIBPATH:/usr/mqm/lib64
fi

#################################################
# for mktpl
export MKTPLDIR=$HOME/mktpl
export MKTPLOS=`uname -a | awk '{print $1}'`
export PATH=$PATH:$HOME/mktpl

#################################################
# for mktpl2
export MKTPL2_HOME=$HOME/mktpl2
export MKTPL2_OS=`uname -a | awk '{print $1}'`
export PATH=$PATH:${MKTPL2_HOME}

################################################
# for svn
export SVN_EDITOR=vi

################################################
# for database
if [ -f $HOME/etc/SetOracleEnv ] ; then
	. $HOME/etc/SetOracleEnv
fi
if [ -f $HOME/etc/DBSetEnv ] ; then
	. $HOME/etc/DBSetEnv
fi
export PROJECT_DIR=$HOME

################################################
# for IB2
# export IB2_SODIR=$HOME/modules
# export IB2_FILEDIR=$HOME/print

################################################
# for IBP
# export IBP_BINDIR=$HOME/bin
# export IBP_SODIR=$HOME/modules
# export IBP_FILEDIR=$HOME/print

################################################
# for dc4c,hzbat
# export DC4C_ACT=act
# export DC4C_OWSERVER=dc4c_owserver
# export DC4C_RSERVERS_IP_PORT=0:12001

################################################
# for SQLST
# export DBTYPE=PGSQL
# export DBHOST=localhost
# export DBPORT=18432
# export DBUSER=calvin
# export DBPASS=calvin
# export DBNAME=calvin

# export PGHOME=/home/idd/dbdata
# export PGDATA=$PGHOME
# export PGHOST=$DBHOST
# export PGPORT=$DBPORT
# export PGDATABASE=$DBNAME
# export PGPASSWORD=$DBPASS
# export PATH=$PATH:/root/local/postgresql/bin:$PATH

# export DBTYPE=MYSQL
# export DBHOST=localhost
# export DBPORT=3306
# export DBUSER=calvin
# export DBPASS=calvin
# export DBNAME=calvindb

# export DBTYPE=PostgreSQL
# export DBHOST=localhost
# export DBPORT=5432
# export DBUSER=calvin
# export DBPASS=calvin
# export DBNAME=calvindb

