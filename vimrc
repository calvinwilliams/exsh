"set term=screen

"set background=dark
"set t_Co=256

if &t_Co > 1
	syntax enable
endif
"filetype on
"filetype plugin on
"filetype indent on

if has ("fdm")
	set fdm=syntax
	set tags=tags;
	set autochdir
endif

if has("autocmd")
       autocmd BufNewFile,Bufread *.log* set syntax=log
endif

autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"set cursorline
"hi CursorLine cterm=underline gui=underline
"set cursorcolumn
"hi CursorColumn ctermbg=blue guibg=blue

"set cursorline
"hi CursorLine cterm=underline gui=underline
"set cursorcolumn
"hi CursorColumn cterm=reverse gui=reverse

" set encoding=gb18030
" set fileencoding=latin1
" set fileencodings=ucs-bom,utf-8,gb18030,gbk,gb2312,cp936
" set fileencoding=utf8
set encoding=utf-8
set fileencodings=ucs-bom,utf-8,gb18030,gbk,gb2312,cp936
" set termencoding=utf8

set tabstop=8
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

autocmd BufNewFile,Bufread *.z setf z

