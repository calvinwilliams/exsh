#!/bin/bash

if [ -d $HOME/exsh ] ; then
	echo "============================================================ git pull all exsh ..."
	cd $HOME/exsh
	gitpullall
	if [ $? -ne 0 ] ; then
		echo "============================================================ git pull all exsh failed"
		exit 1
	else
		echo "============================================================ git pull all exsh ok"
	fi
fi

if [ -d $HOME/mktpl2 ] ; then
	echo "============================================================ git pull all mktpl2 ..."
	cd $HOME/mktpl2
	gitpullall
	if [ $? -ne 0 ] ; then
		echo "============================================================ git pull all mktpl2 failed"
		exit 1
	else
		echo "============================================================ git pull all mktpl2 ok"
	fi
fi

grep -v -E "^#" $HOME/etc/mm_src_all.conf | while read GITREPO ; do
	if [ -d $HOME/src/$GITREPO ] ; then
		echo "============================================================ git pull all $GITREPO ..."
		cd $HOME/src/$GITREPO
		gitpullall
		if [ $? -ne 0 ] ; then
			echo "============================================================ git pull all $GITREPO failed"
			exit 1
		else
			echo "============================================================ git pull all $GITREPO ok"
		fi
	fi
done

