#!/bin/bash

grep -v -E "^#" $HOME/etc/mm_src_all.conf | while read GITREPO ; do
	if [ -d $HOME/src/$GITREPO ] ; then
		echo "============================================================ make $GITREPO ..."
		cd $HOME/src/$GITREPO
		if [ -f Makefile ] ; then
			make Clean && make Install && make Clean
		elif [ -f makefile ] || [ -f makes ] ; then
			mc
		else
			echo "============================================================ No makefile in $GITREPO"
			exit 2
		fi
		if [ $? -ne 0 ] ; then
			echo "============================================================ make $GITREPO failed"
			exit 1
		else
			echo "============================================================ make $GITREPO ok"
		fi
	fi
done

