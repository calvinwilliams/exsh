syn match logDebug /.*|\s\+DEBUG\s\+|.*/
syn match logInfo /.*|\s\+INFO\s\+|.*/
syn match logNotice /.*|\s\+NOTICE\s\+|.*/
syn match logWarn /.*|\s\+WARN\s\+|.*/
syn match logWarn /.*|\s\+Warn\s\+|.*/
syn match logError /.*|\s\+ERROR\s\+|.*/
syn match logError /.*|\s\+Error\s\+|.*/
syn match logFatal /.*|\s\+FATAL\s\+|.*/
syn match logFatal /.*|\s\+Fatal\s\+|.*/
syn match logFatal /^==.*/

hi logDebug ctermfg=Grey guifg=Grey
hi logInfo ctermfg=Cyan guifg=Cyan
hi logNotice ctermfg=DarkCyan guifg=DarkCyan
hi logWarn ctermfg=Yellow guifg=Yellow
hi logError ctermfg=Red guifg=Red
hi logFatal ctermfg=Magenta guifg=Magenta

