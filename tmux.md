tmux实践配置和工具脚本
====================

<!-- TOC -->

- [tmux实践配置和工具脚本](#tmux实践配置和工具脚本)
- [1. tmux是什么](#1-tmux是什么)
- [2. 为什么要写这个实践配置和工具脚本](#2-为什么要写这个实践配置和工具脚本)
- [3. 实践配置](#3-实践配置)
- [4. 工具脚本](#4-工具脚本)
- [5. 最后](#5-最后)

<!-- /TOC -->

# 1. tmux是什么

tmux是一款字符终端多路复用软件，它可以让一个用户客户端连接中创建多个工作窗口甚至窗格，类似于远程终端软件的多标签页能力，特别适合于长期在字符终端上工作的人。

tmux以C/S架构工作，用户客户端连接tmux服务进程，管理多个并行交互窗口。

tmux的一次连接被称为一个会话（session），每个会话可以创建多个窗口（window），每个窗口有一个缺省的全屏的窗格，还可以创建其它窗格（pane）放置在窗口的其它区域。

每个tmux会话连接建立时会读取环境配置文件`~/.tmux.conf`，用于重设快捷键、开启关闭开关、设置选项、设置属性、设置颜色等等。

# 2. 为什么要写这个实践配置和工具脚本

公司里正版化要求，告别我用惯的SecureCRT，尝试了其它免费的字符终端软件，要么界面太丑太杂，要么不能快速切换标签页（必须两个键以内），既然客户端软件选不好就找找其它，偶然机会发现了tmux，试用了一下，惊为天人，其强大的功能简直就是为长期在字符终端上工作的人提供的福器，不过缺省界面丑了点，但它拥有非常丰富的定制能力，于是研究了一翻参考手册，搞出了一个实践配置以及一批工具脚本，优化了功能使用和界面，也分享出来给大家玩玩，至少为大家定制自己的tmux环境提供一些参考。

有了tmux，我就不用再去找字符终端软件了，直接用免费干净简洁的PuTTY既可满足工作需要（主要是能快速切换标签页）。

tmux实践配置和工具脚本由环境配置文件和若干工具脚本组成，包含在我的通用脚本库exsh中。

exsh代码托管在

```
https://gitee.com/calvinwilliams/exsh
```

你可以克隆到自己的环境里，一般放置在`$HOME/exsh`

```
$ cd $HOME
$ git clone https://gitee.com/calvinwilliams/exsh
```

# 3. 实践配置

tmux.conf是tmux的环境配置文件，一般放置在`$HOME/.tmux.conf`。

通用脚本库exsh中带的tmux.conf是我根据自己的需求定制的环境配置。里面包含修改了：
* 我认为更合理的配置选项值；
* tmux前置快捷键从Ctrl+B改成了Ctrl+A；
* 多窗口标签页快速切换快捷键，设置了Alt+数字、Ctrl+数字、F1~F10都能切换，0后面两个键位也赋予了常用功能，比如F11或Ctrl+'-'或Alt+'-'用作当前激活窗口与上一个激活窗口之间互相切换，F12或Ctrl+'='或Alt+'='弹出窗口列表以供选择；
* 状态栏从下面调整到上面、左栏显示会话（session）名，中间栏显示该会话中所有窗口，用不同背景色和前景色提示当前激活窗口和上一个激活窗口，右栏显示版权信息；

来个主界面：

![tmux_ui_1.png](tmux_ui_1.png)

看，像是给PuTTY配上了多窗口标签页，PuTTY可穿透的切换快捷键是Alt+数字，若使用其它字符终端软件，可尝试其它允许穿透到tmux的快捷键，比如SecureCRT已经占用了快捷键Alt+数字，可使用F1~F10发送给tmux。

具体安装方法是，把tmux.conf链接到用户主目录里

```
$ ln -s ~/exsh/tmux.conf ~/.tmux.conf
```

然后，每当建立tmux会话连接时都会从~/.tmux.conf读取环境配置，应用到新会话中。会话工作期间可以用命令`tmux source-file ~/.tmux.conf`通知重载环境配置。

如果你要加入自己的配置，可以直接修改该环境配置文件，也可以把链接文件改成直接复制，然后修改之。

# 4. 工具脚本

tmux会话（session）只保持在内存中，如果重启了就没有了，我又写了脚本tmuxdump用于把会话以及会话内的所有窗口配置都持久化到硬盘上、脚本tmuxrestore从硬盘上的持久化配置文件里重新构建会话到内存中。

具体安装方法是，把exsh路径纳入可执行程序搜索路径中

```
export PATH=$PATH:$HOME/exsh
```

然后，就能在任何目录中执行工具脚本了。

列出内存中当前会话清单

```
$ tmux ls
mdbsrc: 10 windows (created Fri Jul 30 22:40:31 2021) [80x23]
mdbsrc2: 6 windows (created Fri Jul 30 22:40:30 2021) [80x23]
mdbtest: 2 windows (created Fri Jul 30 22:40:32 2021) [80x23]
```

导出会话配置mdbsrc到硬盘

```
$ tmuxdump mdbsrc
tmux session mdbsrc saved to /home/calvin/.tmux/tmuxsession_mdbsrc.windows.
```

导出的会话配置文件实际保存在`$HOME/.tmux/tmuxsession_mdbsrc.windows`

如果tmuxdump不带参数，就导出所有会话

```
$ tmuxdump
tmux session mdbsrc saved to /home/calvin/.tmux/tmuxsession_mdbsrc.windows.
tmux session mdbsrc2 saved to /home/calvin/.tmux/tmuxsession_mdbsrc2.windows.
tmux session mdbtest saved to /home/calvin/.tmux/tmuxsession_mdbtest.windows.
```

重启之后，内存中的会话没有了

```
$ tmux ls
failed to connect to server
```

用硬盘上持久化下来的配置文件，重新构建会话到内存中

可以先看看当初持久化来了哪些会话

```
$ tmuxrestore -l
session "mdbsrc2" found.
session "mdbsrc" found.
session "mdbtest" found.
```

把会话`mdbsrc`重新构建会话到内存中

```
$ tmux ls
failed to connect to server

$ tmuxrestore mdbsrc
session "mdbsrc" restored.
session "mdbsrc" window "1" path "/home/calvin/src/mdb" restored.
session "mdbsrc" window "2" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "3" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "4" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "5" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "6" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "7" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "8" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "9" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "10" path "/home/calvin/src/mdb/src" restored.

$ tmux ls
mdbsrc: 10 windows (created Fri Jul 30 22:49:24 2021) [80x23]
```

或者

重载所有会话构建到内存中

```
$ tmux ls
failed to connect to server

$ tmuxrestore
session "mdbsrc2" restored.
session "mdbsrc2" window "1" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc2" window "2" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc2" window "3" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc2" window "4" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc2" window "5" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc2" window "6" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" restored.
session "mdbsrc" window "1" path "/home/calvin/src/mdb" restored.
session "mdbsrc" window "2" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "3" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "4" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "5" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "6" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "7" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "8" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "9" path "/home/calvin/src/mdb/src" restored.
session "mdbsrc" window "10" path "/home/calvin/src/mdb/src" restored.
session "mdbtest" restored.
session "mdbtest" window "1" path "/home/calvin/src/mdb/test" restored.
session "mdbtest" window "2" path "/home/calvin/src/mdb/test" restored.

$ tmux ls
mdbsrc: 10 windows (created Fri Jul 30 22:49:24 2021) [80x23]
mdbsrc2: 6 windows (created Fri Jul 30 22:49:24 2021) [80x23]
mdbtest: 2 windows (created Fri Jul 30 22:49:25 2021) [80x23]
```

现在，马上进入你的工作吧，不用每次重启完后再手工一个个开窗口了

```
$ tmux a -t mdbsrc
```

![tmux_ui_1.png](tmux_ui_1.png)

enjoy it :)

# 5. 最后

关于作者：厉华，成长在杭州，求学在杭州，工作在杭州，左手C，右手JAVA，写过小到性能卓越方便快捷的日志库、HTTP解析器、日志采集器等，大到交易平台/中间件等，分布式系统实践者，容器技术专研者，2003年大学毕业后一直从事Linux中后台开发，目前在某城市商业银行负责基础架构。

通过邮箱可以联系我 : [网易](mailto:calvinwilliams@163.com)、[Gmail](mailto:calvinwilliams.c@gmail.com)
