#!/bin/bash

if [ $# -ne 1 ] ; then
	echo "gitcheckout_src_all.sh gitbranch"
	exit 1
fi

GITBRANCH=$1

if [ -d $HOME/exsh ] ; then
	echo "============================================================ git checkout $GITBRANCH exsh ..."
	cd $HOME/exsh
	git checkout $GITBRANCH
	if [ $? -ne 0 ] ; then
		echo "============================================================ git checkout $GITBRANCH exsh failed"
		exit 1
	else
		echo "============================================================ git checkout $GITBRANCH exsh ok"
	fi
fi

if [ -d $HOME/mktpl2 ] ; then
	echo "============================================================ git checkout $GITBRANCH mktpl2 ..."
	cd $HOME/mktpl2
	git checkout $GITBRANCH
	if [ $? -ne 0 ] ; then
		echo "============================================================ git checkout $GITBRANCH mktpl2 failed"
		exit 1
	else
		echo "============================================================ git checkout $GITBRANCH mktpl2 ok"
	fi
fi

grep -v -E "^#" $HOME/etc/mm_src_all.conf | while read GITREPO ; do
	if [ -d $HOME/src/$GITREPO ] ; then
		echo "============================================================ git checkout $GITBRANCH $GITREPO ..."
		cd $HOME/src/$GITREPO
		git checkout $GITBRANCH
		if [ $? -ne 0 ] ; then
			echo "============================================================ git checkout $GITBRANCH $GITREPO failed"
			exit 1
		else
			echo "============================================================ git checkout $GITBRANCH $GITREPO ok"
		fi
	fi
done

