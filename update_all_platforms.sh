#!/bin/bash

# FILTER_USER="ib2-ib1-oracle"
# if [ x"$USER" != x"$FILTER_USER" ] ; then
# 	echo "ERROR : user must be '$FILTER_USER'"
# 	exit 1
# fi

function CheckDir()
{
	# DIR=$1
	# if [ ! -d $DIR ] ; then
	# 	echo "expect dir $DIR"
	# 	exit 1
	# fi
	printf ""
}

CheckDir $HOME/exsh
CheckDir $HOME/mktpl2
CheckDir $HOME/src/tools
CheckDir $HOME/src/fasterjson
CheckDir $HOME/src/DirectStruct
CheckDir $HOME/src/InterBankPlus
CheckDir $HOME/src/ib1-ibp
CheckDir $HOME/src/ib2-ibp
CheckDir $HOME/src/ttm-ib2
CheckDir $HOME/src/dc4c
CheckDir $HOME/src/hzbat
CheckDir $HOME/src/logpipe
CheckDir $HOME/src/fileServer
CheckDir $HOME/src/nsproxy

if [ -d $HOME/exsh ] ; then
	echo "============================================================ update exsh ..."
	cd $HOME/exsh
	svn up
	if [ $? -ne 0 ] ; then
		echo "============================================================ update exsh failed"
		exit 1
	else
		echo "============================================================ update exsh ok"
	fi
fi

if [ -d $HOME/mktpl2 ] ; then
	echo "============================================================ update mktpl2 ..."
	cd $HOME/mktpl2
	git pull
	if [ $? -ne 0 ] ; then
		echo "============================================================ update mktpl2 failed"
		exit 1
	else
		echo "============================================================ update mktpl2 ok"
	fi
fi

if [ -d $HOME/src/tools ] ; then
	echo "============================================================ update tools ..."
	cd $HOME/src/tools
	svn up
	if [ $? -ne 0 ] ; then
		echo "============================================================ update tools failed"
		exit 1
	else
		echo "============================================================ update tools ok"
	fi
fi

if [ -d $HOME/src/fasterjson ] ; then
	echo "============================================================ update fasterjson ..."
	cd $HOME/src/fasterjson
	git pull
	if [ $? -ne 0 ] ; then
		echo "============================================================ update fasterjson failed"
		exit 1
	else
		echo "============================================================ update fasterjson ok"
	fi
fi

if [ -d $HOME/src/DirectStruct ] ; then
	echo "============================================================ update DirectStruct ..."
	cd $HOME/src/DirectStruct
	git pull
	if [ $? -ne 0 ] ; then
		echo "============================================================ update DirectStruct failed"
		exit 1
	else
		echo "============================================================ update DirectStruct ok"
	fi
fi

if [ -d $HOME/src/InterBankPlus ] ; then
	echo "============================================================ update InterBankPlus ..."
	cd $HOME/src/InterBankPlus
	git pull
	if [ $? -ne 0 ] ; then
		echo "============================================================ update InterBankPlus failed"
		exit 1
	else
		echo "============================================================ update InterBankPlus ok"
	fi
fi

if [ -d $HOME/src/ib1-ibp ] ; then
	echo "============================================================ update ib1-ibp ..."
	cd $HOME/src/ib1-ibp
	svn up
	if [ $? -ne 0 ] ; then
		echo "============================================================ update ib1-ibp failed"
		exit 1
	else
		echo "============================================================ update ib1-ibp ok"
	fi
fi

if [ -d $HOME/src/ib2-ibp ] ; then
	echo "============================================================ update ib2-ibp ..."
	cd $HOME/src/ib2-ibp
	svn up
	if [ $? -ne 0 ] ; then
		echo "============================================================ update ib2-ibp failed"
		exit 1
	else
		echo "============================================================ update ib2-ibp ok"
	fi
fi

if [ -d $HOME/src/ttm-ib2 ] ; then
	echo "============================================================ update ttm-ib2 ..."
	cd $HOME/src/ttm-ib2
	svn up
	if [ $? -ne 0 ] ; then
		echo "============================================================ update ttm-ib2 failed"
		exit 1
	else
		echo "============================================================ update ttm-ib2 ok"
	fi
fi

if [ -d $HOME/src/dc4c ] ; then
	echo "============================================================ update dc4c ..."
	cd $HOME/src/dc4c
	git pull
	if [ $? -ne 0 ] ; then
		echo "============================================================ update dc4c failed"
		exit 1
	else
		echo "============================================================ update dc4c ok"
	fi
fi

if [ -d $HOME/src/hzbat ] ; then
	echo "============================================================ update hzbat ..."
	cd $HOME/src/hzbat
	git pull
	if [ $? -ne 0 ] ; then
		echo "============================================================ update hzbat failed"
		exit 1
	else
		echo "============================================================ update hzbat ok"
	fi
fi

if [ -d $HOME/src/logpipe ] ; then
	echo "============================================================ update logpipe ..."
	cd $HOME/src/logpipe
	git pull
	if [ $? -ne 0 ] ; then
		echo "============================================================ update logpipe failed"
		exit 1
	else
		echo "============================================================ update logpipe ok"
	fi
fi

if [ -d $HOME/src/fileServer ] ; then
	echo "============================================================ update fileServer ..."
	cd $HOME/src/fileServer
	svn up
	if [ $? -ne 0 ] ; then
		echo "============================================================ update fileServer failed"
		exit 1
	else
		echo "============================================================ update fileServer ok"
	fi
fi

if [ -d $HOME/src/nsproxy ] ; then
	echo "============================================================ update nsproxy ..."
	cd $HOME/src/nsproxy
	git pull
	if [ $? -ne 0 ] ; then
		echo "============================================================ update nsproxy failed"
		exit 1
	else
		echo "============================================================ update nsprxoy ok"
	fi
fi
