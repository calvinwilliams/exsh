echo -e "\033[2J\033[0m"

echo "--------- Text attributes ---------"
A=0
while [ $A -le 8 ] ; do
	if [ $A -eq 0 ] || [ $A -eq 1 ] || [ $A -eq 4 ] || [ $A -eq 5 ] || [ $A -eq 7 ] || [ $A -eq 8 ] ; then
		printf "${A}	\033[${A}mTEXT\033[0m\n"
	fi
	A=`expr $A + 1`
done

echo "--------- Text colors ---------"
printf "F-B"
B=40
while [ $B -le 47 ] ; do
	printf "\t${B}"
	B=`expr $B + 1`
done
printf "\n"

F=30
while [ $F -le 37 ] ; do
	printf "${F}"
	B=40
	while [ $B -le 47 ] ; do
		printf "\t\033[${F};${B}mTEXT\033[0m"
		B=`expr $B + 1`
	done
	printf "\n"
	F=`expr $F + 1`
done

