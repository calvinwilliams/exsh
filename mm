function make_clean_and_make_prepro_all_make_install()
{
	make clean $*
	NRET=$?
	if [ $NRET -ne 0 ] ; then
		exit $NRET
	fi
	
	make prepro_all $*
	NRET=$?
	if [ $NRET -ne 0 ] ; then
		exit $NRET
	fi
	
	CPUS=`cat /proc/cpuinfo | grep -w processor | wc -l`
	make install -j $CPUS $*
	NRET=$?
	if [ $NRET -ne 0 ] ; then
		exit $NRET
	fi
}

if [ -f makes ] ; then
	for DIR in `cat makes` ; do
		cd $DIR
		make_clean_and_make_prepro_all_make_install
		cd ..
	done
else
	make_clean_and_make_prepro_all_make_install
fi

