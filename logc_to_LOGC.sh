FILE=$1

cat ${FILE} | sed 's/FatalLogc( __FILE__ , __LINE__ ,/FATALLOGC(/g' | \
	sed 's/ErrorLogc( __FILE__ , __LINE__ ,/ERRORLOGC(/g' | \
	sed 's/WarnLogc( __FILE__ , __LINE__ ,/WARNLOGC(/g' | \
	sed 's/InfoLogc( __FILE__ , __LINE__ ,/INFOLOGC(/g' | \
	sed 's/DebugLogc( __FILE__ , __LINE__ ,/DEBUGLOGC(/g' | \
	sed 's/FatalHexLogc( __FILE__ , __LINE__ ,/FATALHEXLOGC(/g' | \
	sed 's/ErrorHexLogc( __FILE__ , __LINE__ ,/ERRORHEXLOGC(/g' | \
	sed 's/WarnHexLogc( __FILE__ , __LINE__ ,/WARNHEXLOGC(/g' | \
	sed 's/InfoHexLogc( __FILE__ , __LINE__ ,/INFOHEXLOGC(/g' | \
	sed 's/DebugHexLogc( __FILE__ , __LINE__ ,/DEBUGHEXLOGC(/g' | \
	sed 's/\(.*\)\(LOGC(\)\(.*\);/\1\2\3/g' >${FILE}.tmp
mv ${FILE}.tmp ${FILE}

