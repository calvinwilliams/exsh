FILE=$1

cat ${FILE} | sed 's/\<FATALLOG\>/FATALLOGC/g' | \
	sed 's/\<ERRORLOG\>/ERRORLOGC/g' | \
	sed 's/\<WARNLOG\>/WARNLOGC/g' | \
	sed 's/\<INFOLOG\>/INFOLOGC/g' | \
	sed 's/\<DEBUGLOG\>/DEBUGLOGC/g' | \
	sed 's/\<FATALHexLOG\>/FATALHEXLOGC/g' | \
	sed 's/\<ERRORHEXLOG\>/ERRORHEXLOGC/g' | \
	sed 's/\<WARNHEXLOG\>/WARNHEXLOGC/g' | \
	sed 's/\<INFOHEXLOG\>/INFOHEXLOGC/g' | \
	sed 's/\<DEBUGHEXLOG\>/DEBUGHEXLOGC/g' | \
	sed 's/\(.*\)\(LOGC(\)\(.*\);/\1\2\3/g' >${FILE}.tmp
mv ${FILE}.tmp ${FILE}

