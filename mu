function make_unintall()
{
	make uninstall $*
	NRET=$?
	if [ $NRET -ne 0 ] ; then
		exit $NRET
	fi
}

if [ -f makes ] ; then
	for DIR in `cat makes` ; do
		cd $DIR
		make_unintall
		cd ..
	done
else
	make_unintall
fi

