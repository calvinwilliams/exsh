DATE=`date +'%Y%m%d'`

if [ $# -lt 2 ] ; then
	echo "USAGE : dumpdb_daily.sh database_name reserve_days"
	exit 7
fi
DATABASE_NAME=$1
RESERVE_DAYS=$2

BACKUP_PATH="$HOME/backup"

if [ ! -d $BACKUP_PATH ] ; then
	echo "ERR : NO $HOME/backup"
	exit 1
fi

cd $BACKUP_PATH
pg_dump -Fc calvin > PGSQL_${DATABASE_NAME}_${DATE}.dump
find . -name "*.dump" -mtime +${RESERVE_DAYS} -exec rm {} \;

