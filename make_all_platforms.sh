#!/bin/bash

# FILTER_USER="ib2-ib1-oracle"
# if [ x"$USER" != x"$FILTER_USER" ] ; then
# 	echo "ERROR : user must be '$FILTER_USER'"
# 	exit 1
# fi

function CheckDir()
{
	# DIR=$1
	# if [ ! -d $DIR ] ; then
	# 	echo "expect dir $DIR"
	# 	exit 1
	# fi
	printf ""
}

CheckDir $HOME/src/tools
CheckDir $HOME/src/fasterjson
CheckDir $HOME/src/DirectStruct
CheckDir $HOME/src/InterBankPlus
CheckDir $HOME/src/ib1-ibp
CheckDir $HOME/src/ib2-ibp
CheckDir $HOME/src/ttm-ib2
CheckDir $HOME/src/dc4c
CheckDir $HOME/src/hzbat
CheckDir $HOME/src/logpipe
CheckDir $HOME/src/fileServer
CheckDir $HOME/src/nsproxy

if [ -d $HOME/src/tools/src ] ; then
	echo "============================================================ build tools ..."
	cd $HOME/src/tools/src
	make Clean && make Install && make Clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build tools failed"
		exit 1
	else
		echo "============================================================ build tools ok"
	fi
fi

if [ -d $HOME/src/fasterjson/src ] ; then
	echo "============================================================ build fasterjson ..."
	cd $HOME/src/fasterjson/src
	make -f makefile.Linux clean && make -f makefile.Linux install && make -f makefile.Linux clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build fasterjson failed"
		exit 1
	else
		echo "============================================================ build fasterjson ok"
	fi
fi

if [ -d $HOME/src/DirectStruct/src ] ; then
	echo "============================================================ build DirectStruct ..."
	cd $HOME/src/DirectStruct/src
	make -f makefile.Linux clean && make -f makefile.Linux install && make -f makefile.Linux clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build DirectStruct failed"
		exit 1
	else
		echo "============================================================ build DirectStruct ok"
	fi
fi

if [ -d $HOME/src/InterBankPlus ] ; then
	echo "============================================================ build InterBankPlus ..."
	cd $HOME/src/InterBankPlus
	make clean && make prepro_all && make install && make clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build InterBankPlus failed"
		exit 1
	else
		echo "============================================================ build InterBankPlus ok"
	fi
fi

if [ -d $HOME/src/ib1-ibp/ABIB.IBP ] ; then
	echo "============================================================ build ib1-ibp ..."
	cd $HOME/src/ib1-ibp/ABIB.IBP
	make Clean && make Install && make Clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build ib1-ibp failed"
		exit 1
	else
		echo "============================================================ build ib1-ibp ok"
	fi
fi

if [ -d $HOME/src/ib2-ibp ] ; then
	echo "============================================================ build ib2-ibp ..."
	cd $HOME/src/ib2-ibp
	make clean && make install && make clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build ib2-ibp failed"
		exit 1
	else
		echo "============================================================ build ib2-ibp ok"
	fi
fi

if [ -d $HOME/src/ttm-ib2 ] ; then
	echo "============================================================ build ttm-ib2 ..."
	cd $HOME/src/ttm-ib2
	make clean && make install && make clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build ttm-ib2 failed"
		exit 1
	else
		echo "============================================================ build ttm-ib2 ok"
	fi
fi

if [ -d $HOME/src/dc4c ] ; then
	echo "============================================================ build dc4c ..."
	cd $HOME/src/dc4c
	make clean && make prepro_all && make install && make clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build dc4c failed"
		exit 1
	else
		echo "============================================================ build dc4c ok"
	fi
fi

if [ -d $HOME/src/hzbat ] ; then
	echo "============================================================ build hzbat ..."
	cd $HOME/src/hzbat
	make clean && make prepro_all && make install && make clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build hzbat failed"
		exit 1
	else
		echo "============================================================ build hzbat ok"
	fi
fi

if [ -d $HOME/src/logpipe ] ; then
	echo "============================================================ build logpipe ..."
	cd $HOME/src/logpipe
	make clean && make prepro_all && make install && make clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build logpipe failed"
		exit 1
	else
		echo "============================================================ build logpipe ok"
	fi
fi

if [ -d $HOME/src/fileServer ] ; then
	echo "============================================================ build fileserver ..."
	cd $HOME/src/fileServer
	make Clean  && make Install && make Clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build fileserver failed"
		exit 1
	else
		echo "============================================================ build fileserver ok"
	fi
fi

if [ -d $HOME/src/nsproxy ] ; then
	echo "============================================================ build nsproxy ..."
	cd $HOME/src/nsproxy
	make clean && make prepro_all && make install && make clean
	if [ $? -ne 0 ] ; then
		echo "============================================================ build nsproxy failed"
		exit 1
	else
		echo "============================================================ build nsproxy ok"
	fi
fi
