#!/bin/bash

# FILTER_USER="ib2-ib1-oracle"
# if [ x"$USER" != x"$FILTER_USER" ] ; then
# 	echo "ERROR : user must be '$FILTER_USER'"
# 	exit 1
# fi

if [ $# -ne 1 ] ; then
	echo "USAGE : checkout_all_platforms.sh (branch)"
	exit 1
fi
BRANCH=$1

function CheckDir()
{
	# DIR=$1
	# if [ ! -d $DIR ] ; then
	# 	echo "expect dir $DIR"
	# 	exit 1
	# fi
	printf ""
}

CheckDir $HOME/mktpl2
CheckDir $HOME/src/fasterjson
CheckDir $HOME/src/DirectStruct
CheckDir $HOME/src/InterBankPlus
CheckDir $HOME/src/dc4c
CheckDir $HOME/src/hzbat
CheckDir $HOME/src/logpipe

if [ -d $HOME/mktpl2 ] ; then
	echo "============================================================ checkout $BRANCH mktpl2 ..."
	cd $HOME/mktpl2
	git checkout $BRANCH
	if [ $? -ne 0 ] ; then
		echo "============================================================ checkout $BRANCH mktpl2 failed"
		exit 1
	else
		echo "============================================================ checkout $BRANCH mktpl2 ok"
	fi
fi

if [ -d $HOME/src/fasterjson ] ; then
	echo "============================================================ checkout $BRANCH fasterjson ..."
	cd $HOME/src/fasterjson
	git checkout $BRANCH
	if [ $? -ne 0 ] ; then
		echo "============================================================ checkout $BRANCH fasterjson failed"
		exit 1
	else
		echo "============================================================ checkout $BRANCH fasterjson ok"
	fi
fi

if [ -d $HOME/src/DirectStruct ] ; then
	echo "============================================================ checkout $BRANCH DirectStruct ..."
	cd $HOME/src/DirectStruct
	git checkout $BRANCH
	if [ $? -ne 0 ] ; then
		echo "============================================================ checkout $BRANCH DirectStruct failed"
		exit 1
	else
		echo "============================================================ checkout $BRANCH DirectStruct ok"
	fi
fi

if [ -d $HOME/src/InterBankPlus ] ; then
	echo "============================================================ checkout $BRANCH InterBankPlus ..."
	cd $HOME/src/InterBankPlus
	git checkout $BRANCH
	if [ $? -ne 0 ] ; then
		echo "============================================================ checkout $BRANCH InterBankPlus failed"
		exit 1
	else
		echo "============================================================ checkout $BRANCH InterBankPlus ok"
	fi
fi

if [ -d $HOME/src/dc4c ] ; then
	echo "============================================================ checkout $BRANCH dc4c ..."
	cd $HOME/src/dc4c
	git checkout $BRANCH
	if [ $? -ne 0 ] ; then
		echo "============================================================ checkout $BRANCH dc4c failed"
		exit 1
	else
		echo "============================================================ checkout $BRANCH dc4c ok"
	fi
fi

if [ -d $HOME/src/hzbat ] ; then
	echo "============================================================ checkout $BRANCH hzbat ..."
	cd $HOME/src/hzbat
	git checkout $BRANCH
	if [ $? -ne 0 ] ; then
		echo "============================================================ checkout $BRANCH hzbat failed"
		exit 1
	else
		echo "============================================================ checkout $BRANCH hzbat ok"
	fi
fi

if [ -d $HOME/src/logpipe ] ; then
	echo "============================================================ checkout $BRANCH logpipe ..."
	cd $HOME/src/logpipe
	git checkout $BRANCH
	if [ $? -ne 0 ] ; then
		echo "============================================================ checkout $BRANCH logpipe failed"
		exit 1
	else
		echo "============================================================ checkout $BRANCH logpipe ok"
	fi
fi

