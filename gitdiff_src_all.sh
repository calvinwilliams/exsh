#!/bin/bash

if [ $# -ne 2 ] ; then
	echo "gitdiff_src_all.sh gitbranch1 gitbranch2"
	exit 1
fi

GITBRANCH1=$1
GITBRANCH2=$1

if [ -d $HOME/exsh ] ; then
	echo "============================================================ git checkout $GITBRANCH1 $GITBRANCH2 exsh ..."
	cd $HOME/exsh
	git diff $GITBRANCH1 $GITBRANCH2
	if [ $? -ne 0 ] ; then
		echo "============================================================ git checkout $GITBRANCH1 $GITBRANCH2 exsh failed"
		exit 1
	else
		echo "============================================================ git checkout $GITBRANCH1 $GITBRANCH2 exsh ok"
	fi
fi

if [ -d $HOME/mktpl2 ] ; then
	echo "============================================================ git checkout $GITBRANCH1 $GITBRANCH2 mktpl2 ..."
	cd $HOME/mktpl2
	git diff $GITBRANCH1 $GITBRANCH2
	if [ $? -ne 0 ] ; then
		echo "============================================================ git checkout $GITBRANCH1 $GITBRANCH2 mktpl2 failed"
		exit 1
	else
		echo "============================================================ git checkout $GITBRANCH1 $GITBRANCH2 mktpl2 ok"
	fi
fi

grep -v -E "^#" $HOME/etc/mm_src_all.conf | while read GITREPO ; do
	if [ -d $HOME/src/$GITREPO ] ; then
		echo "============================================================ git checkout $GITBRANCH1 $GITBRANCH2 $GITREPO ..."
		cd $HOME/src/$GITREPO
		git diff $GITBRANCH1 $GITBRANCH2
		if [ $? -ne 0 ] ; then
			echo "============================================================ git diff $GITBRANCH1 $GITBRANCH2 $GITREPO failed"
			exit 1
		else
			echo "============================================================ git checkout $GITBRANCH1 $GITBRANCH2 $GITREPO ok"
		fi
	fi
done

