echo "--- start mariadb ---------"
systemctl start mariadb
if [ $? -eq 0 ] ; then
	echo "--- start mariadb --------- ok"
else
	echo "--- start mariadb --------- failed"
fi

sleep 1

echo "--- start postgresql ---------"
su --login -c "db.start" postgres
if [ $? -eq 0 ] ; then
	echo "--- start postgresql --------- ok"
else
	echo "--- start postgresql --------- failed"
fi

sleep 1

echo "--- start apache2 ---------"
apache2.start
if [ $? -eq 0 ] ; then
	echo "--- start apache2 --------- ok"
else
	echo "--- start apache2 --------- failed"
fi

