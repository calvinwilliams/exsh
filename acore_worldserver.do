usage()
{
	echo "USAGE : acore_worldserver.do [ start | stop | notify_then_stop | restart | update | saveall ]"
}

run()
{
	echo "$*"
	$*
}

if [ $# -eq 0 ] ; then
	usage
	exit 7 ;
fi

case $1 in
	start)
		echo "--------- 真worldserver ---------"
		tmux new -s worldserver -d
		tmux send -t worldserver "cd ~/azeroth-server/bin && ./worldserver" ENTER
		;;
	stop)
		echo "--------- 真worldserver ---------"
		acore_worldserver.do saveall
		tmux send -t worldserver "server exit" ENTER
		sleep 3
		tmux kill-session -t worldserver
		;;
	notify_then_stop)
		echo "--------- 真真真�worldserver ---------"
		acore_worldserver.do saveall
		tmux send -t worldserver "server shutdown 300" ENTER
		sleep 270
		acore_worldserver.do saveall
		sleep 30
		sleep 3
		tmux kill-session -t worldserver
		;;
	restart)
		acore_worldserver.do stop
		sleep 1
		acore_worldserver.do start
		;;
	update)
		echo "--------- 真真�worldserver ---------"
		if [ -f $HOME/azeroth-server/bin.update/worldserver ] ; then
			run rm -f $HOME/azeroth-server/bin/worldserver
			run mv -f $HOME/azeroth-server/bin.update/worldserver $HOME/azeroth-server/bin/worldserver
		fi
		;;
	saveall)
		echo "--------- 真真真 ---------"
		tmux send -t worldserver "saveall" ENTER
		;;
esac

